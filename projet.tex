\documentclass[french,a4paper,draft]{article}
\usepackage[T1]{fontenc}
\usepackage{
	babel,
	amsmath,
%	amsthm,
	tikz-cd,
	syntonly,
}
%\syntaxonly

\newcommand*{\term}[1]{\emph{#1}}

\newcommand*{\from}{\colon}
\newcommand*{\tq}{\mid}
\newcommand*{\cat}[1]{\mathsf{#1}}
\newcommand*{\op}[1]{#1^{\mathsf{op}}}
\newcommand*{\parties}{\mathcal{P}}

\newcommand*{\Set}{\cat{Set}}

\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\cod}{cod}

\title{Esquisse d'un projet}
\author{Louis de~Forcrand}

\begin{document}

\maketitle

\begin{abstract}
	Nous donnons la définition d'une construction catégorique
	qui est une vaste généralisation de la grande majorité
	des structures mathématiques couramment utilisées,
	et nous survolons un plan d'attaque afin de l'étudier.
\end{abstract}

% Expliquer la motivation intuitive quelque part.
\section{La définition}

Soit \( U \from \cat{J} \to \cat{C} \) un foncteur entre deux catégories.
Nous associons à chaque choix de
\begin{itemize}
\item	une catégorie~$\cat{D}$,
\item	des foncteurs \( P,Q \from \cat{C} \to \cat{D} \),
\item	et une transformation naturelle \( \tau \from PU \to QU \)
\end{itemize}
une catégorie ayant comme objets ceux de~$\cat{J}$,
et les morphismes de~$\cat{C}$ qui préservent la structure de~$\tau$:
\begin{equation}\label{eq def u_tau}
	\hom(j,k) = \{\, f \from Uj \to Uk
		\tq Qf\, \tau_{j} = \tau_{k}\, Pf \,\}.
\end{equation}
Nous l'appellerons~$U_{\tau}$,
la \term{catégorie préservante} par~$U$ de~$\tau$.

Nous avons donc une catégorie avec les objets de~$\cat{J}$
et certains morphismes de~$\cat{C}$;
nous écrirons \( \ddot{U} = \{U_{\tau}\}_{\tau} \) pour l'ensemble
des catégories générées par le processus précédent.
Plus généralement
nous écrirons $\bar{U}$ pour l'ensemble des catégories~$\cat{u}$
avec les objets de~$\cat{J}$ et des morphismes de~$\cat{C}$:
\[
	\hom_{\cat{u}}(j,k) \subseteq \hom_{C}(Uj,Uk).
\]

L'ensemble~$\bar{U}$ admet une structure naturelle de treillis complet:
\begin{equation}\label{eq def u treillis}
	\cat{u} \le \cat{v} \quad \text{si} \quad
		\hom_{\cat{u}}(j,k) \subseteq \hom_{\cat{v}}(j,k).
\end{equation}
Le sous-ensemble \( \ddot{U} \subseteq \bar{U} \)
est ordonné de la même manière mais n'est à priori pas un treillis.
L'un des buts principaux de ce projet est d'étudier la relation
entre $\ddot{U}$~et~$\bar{U}$.

\section{Quelques exemples}

Nous donnons tout d'abord un bon nombre d'exemples,
afin d'illustrer la généralité des résultats qui seraient obtenus
au travers de l'étude de ces catégories préservantes.

\subsection{Les espaces algébriques}

L'étude des espaces algébriques a mené
à la définition des catégories préservantes,
qui sont une généralisation légère des espaces algébriques.

Une catégorie d'espaces algébriques est une sous-catégorie~$\cat{A}$
d'une catégorie~$\cat{C}$ munie d'un foncteur \( P \from \cat{C} \to \cat{C} \),
avec pour chaque objet \( A \in \cat{A} \subseteq \cat{C} \)
un morphisme \( \eta_{A} \from PA \to A \).
Les morphismes de~$\cat{A}$ sont ceux de~$C$ qui commutent avec les~$\eta_{A}$:
\[
	f\, \eta_{A} = \eta_{B}\, Pf.
\]

Cette condition est très similaire à celle de l'Équation~\eqref{eq def u_tau};
on voit que la seule différence est l'identification de~$\eta$
en tant que transformation naturelle,
ainsi que l'ajout de la catégorie supplémentaire~$\cat{D}$ et le foncteur~$Q$.

\subsection{Les graphes}

Un graphe (orienté) est un ensemble~$V$ de sommets
ainsi qu'un ensemble~$E$ d'arêtes entre les sommets.
Chaque arête a donc un début et une fin,
que l'on appelera son \term{domaine} et \term{codomaine}.
Un morphisme de graphes est une paire de fonctions
entre les arêtes et les sommets des graphes,
qui préservent les domaines et codomaines des arêtes.

Formellement nous dirons donc qu'un \term{graphe}
est une paire d'ensembles \( G = (V,E) \)
munie de deux applications \( \dom_{G},\cod_{G} \from E \to V \).
On prend donc $\cat{J}$ la collection de tous les graphes.

Les morphismes de graphes sont des fonctions
entre les ensembles constituant respectivement chacun des graphes;
ils vivent dans une catégorie où les objets
sont des paires d'éléments de~$\Set$,
et les morphismes sont des paires de fonctions.
Nous prenons cette catégorie pour~$\cat{C}$.

La structure d'un graphe que doivent conserver les morphismes
est déterminée par ses fonctions $\dom_{G}$~et~$\cod_{G}$.
Ces derniers vivent dans~$\Set$,
que nous choisissons donc pour~$\cat{D}$.
Il nous faut maintenant définir les deux foncteurs $P$~et~$Q$.
Nous prenons
\begin{align*}
	P(A,B) &= A,	\\
	Q(A,B) &= B^{2}.
\end{align*}
Ce choix est le bon puisque les fonctions de domaine et codomaine d'un graphe
vont d'un membre de la paire~$(V,E)$ vers l'autre.
Nous prenons le produit cartésien \( B \times B \)
car nous avons deux fonctions à préserver.

Il ne nous reste qu'une partie des données d'un graphe à utiliser
pour construire la transformation naturelle~$\tau$,
notamment les fonctions $\dom_{G}$~et~$\cod_{G}$.
Ces dernières encodent la \emph{relation} entre les arêtes et les sommets;
elle recolle chaque arête sur son départ et son arrivée,
et les morphismes de graphes devront respecter ce recollement.
On prend
\[
	\tau_{G} = \dom_{G} \times \cod_{G}
		\from E \to V^{2}.
\]

Les morphismes de la catégorie préservante~$U_{\tau}$
selon l'Équation~\eqref{eq def u_tau} sont alors précisément
ceux qui commutent avec les applications domaine et codomaine.

Notons que nous aurions également pu prendre
\( (A,B) \mapsto B \sqcup B \) pour~$P$,
simplement \( (A,B) \mapsto A \) pour~$Q$,
et remplacer $\tau_{G}$ par \( \dom_{G} \sqcup \cod_{G} \).

\subsection{Les relations}

Un type de graphe particulier est celui qui a au plus une arête
entre chaque paire de sommets.
Ces graphes \term{booléens} sont équivalents aux relations binaires.
Fixons plus généralement un cardinal~$N$.
Une relation d'arité~$N$ sur un ensemble~$X$ est un sous-ensemble
\[
	R \subseteq X^{N}.
\]
Une fonction \( X \to Y \) entre ensembles munis respectivement
de relations $R$~et~$S$ d'arité~$N$ qui les préserve
va envoyer $R$ dans~$S$.

Formellement $\cat{J}$ sera une collection d'ensembles
munis de relations bien choisies (par exemple des ensembles bien ordonnés),
$\cat{C}$ sera encore la catégorie des paires d'ensembles,
et $\cat{D}$ simplement les ensembles,~$\Set$.
Ainsi nous pouvons voir un ensemble~$X$
muni d'une relation \( R \subseteq X^{N} \)
comme une paire d'ensembles~$(X,R)$ au travers du foncteur~$U$,
et nos morphismes \( (f,g) \from (X,R) \to (Y,S) \)
vont envoyer les éléments de~$X$ sur ceux de~$Y$,
tout en transportant les relations de~$X$ sur celles de~$Y$.

Sous ce foncteur~$U$ d'oubli,
tout morphisme va préserver les relations puisque sa composante
sur les relations est \( g \from R \to S \).
Or nous n'avons toujours pas déterminé comment obliger
la fonction \( f \from X \to Y \) à préserver les relations,
autrement dit à demander que \( f(R) \subseteq S \).
Pour ce faire nous utilisons la même technique qu'avec les graphes,
qui est de dire comment \frquote{recoller} une relation sur son ensemble.
La solution est simplement d'identifier~$R$ comme un sous-ensemble de~$X^{N}$:
\begin{align*}
	P(A,B) &= B,	\\
	Q(A,B) &= A^{N},	\\
	\tau_{(X,R)} &= R \hookrightarrow X^{N}.
\end{align*}

L'inclusion existe car $\tau$ est une transformation naturelle en~$\cat{J}$,
qui contient précisément des paires~$(X,R)$ avec \( R \subseteq X^{N} \).
Demander la commutation de l'Équation~\eqref{eq def u_tau}
pour \( (f,g) \from (X,R) \to (Y,S) \) revient à demander
que \( f^{N} \from X^{N} \to Y^{N} \) se restreigne à \( g \from R \to S \),
ce qui arrive si et seulement si
\[
	(x_{n})_{n\in N} \in R \quad \text{implique} \quad
	\bigl( f(x_{n}) \bigr)_{n\in N} \in S.
\]
% Dire que ceci englobe les espaces algébriques anciens d'arités {N}.

\subsection{Les espaces topologiques}

La méthode utilisée dans la section précédente
permet avec de légères modifications de voir les espaces algébriques
ainsi que les espaces topologiques sous le même formalisme
des catégories préservantes,
et d'expliquer pourquoi les fonctions continues
préservent naturellement la relation d'arité~$1$ d'être ouvert,
mais dans le sens opposé.

On prend $\cat{J}$ les espaces topologiques.
La catégorie~$\cat{C}$ est plus originale pour cet exemple:
ses objets sont des paires d'ensembles,
mais un morphisme \( (X,T) \to (Y,S) \) est une paire~$(f,t)$ avec
\begin{align*}
	f &\from X \to T,	\\
	t &\from Y \leftarrow S,
\end{align*}
ceci car une fonction continue~$f$ va transporter
les éléments de~$X$ sur ceux de~$Y$ mais les ouverts de~$Y$ sur ceux de~$X$.

La catégorie~$\cat{D}$ est définie comme~$\op{\Set}$,
et les deux foncteurs sont
\begin{align*}
	P(X,T) &= \op{\parties(X)},	\\
	Q(X,T) &= T,	\\
	\tau_{(X,T)} &= \op{( T \hookrightarrow \parties(X) )}.
\end{align*}
En renversant le diagramme dans \( D = \op{\Set} \)
de l'Équation~\eqref{eq def u_tau} pour le voir dans~$\Set$,
on obtient pour \( (f,t) \from (X,T_{X}) \to (Y,T_{Y}) \):
\[
	\begin{tikzcd}
	T_{Y} \ar[r, "t"] \ar[d, hook]	& T_{X} \ar[d, hook]	\\
	\parties(Y) \ar[r, "f^{-1}"]	& \parties(X).
	\end{tikzcd}
\]
La commutation de ce diagramme veut dire,
comme dans l'exemple précédent des ensembles munis de relations,
que l'application \( f^{-1} \from \parties(Y) \to \parties(X) \)
se restreint en \( t \from T_{Y} \to T_{X} \);
autrement dit que \( f^{-1}(U) \in T_{X} \) pour tout \( U \in T_{Y} \).

\section{Le cas général des foncteurs vers~$\Set$}

La similitude des deux exemples précédents
conduit à la généralisation suivante,
qui donne une explication naturelle à par exemple
pourquoi les relations d'un graphe ou d'un espace algébrique
sont préservées dans le même sens
que les morphismes de la catégorie correspondante,
alors que les \frquote{relations} des espaces topologiques
sont préservées dans le sens opposé.

Soit \( F \from \Set \to \Set \) un foncteur \emph{covariant} vers~$\Set$.
On considère une collection~$\cat{J}$ d'ensembles,
chacun muni d'un sous-ensemble de son image par~$F$.
Notons ses éléments \( J \in \cat{J} \)
et les parties correspondantes de leurs images \( R_{J} \subseteq F(J) \).
Nous pouvons alors créer la catégorie préservante
définie par les foncteurs suivants \( \Set \to \Set^{2} \):
\begin{align*}
	P(X,R) &= R,	\\
	Q(X,R) &= Q,	\\
	\tau_{J} &= R_{J} \hookrightarrow F(J).
\end{align*}
Identiquement aux deux exemples qui précèdent,
les morphismes de cette catégorie \( J \to K \)
sont les fonctions \( f \from J \to K \)
telles que \( F(f)(R_{J}) \subseteq F(f)(R_{K}) \).

Supposons maintenant \( F \from \Set \to \op{\Set} \) \emph{contravariant}.
On considère à nouveau $\cat{J}$ comme des ensembles~$J$
munis de sous-ensembles \( R_{J} \subseteq F(J) \).
Dans ce cas par contre nous procédons comme pour les espaces topologiques,
et nous prenons \( D = \Set \times \op{\Set} \):
\begin{align*}
	P(X,R) &= R,	\\
	Q(X,R) &= F(Q),	\\
	\tau_{J} &= \op{( R_{J} \hookrightarrow F(J) )}.
\end{align*}
On obtient le même diagramme pour l'Équation~\eqref{eq def u_tau}
que dans le cas topologique,
avec le même type de morphismes \( J \to K \):
les fonctions \( f \from J \to K \)
avec \( F(f)(R_{K}) \subseteq F(f)(R_{J}) \).

Ces deux constructions se généralisent de la façon évidente
à des catégories autres que~$\Set$.
Dans ce cadre général,
les foncteurs intéressants les plus faciles à trouver
et qui sont présents dans toute catégorie
% localement petite
sont ceux de la pré- et post-composition.
En fait, les deux exemples
des ensembles munis de relations et des espaces topologiques
sont précisément les cas de la construction générale donnée dans cette section
appliquée à~$\hom(N,-)$ et~$\hom(-,2)$.

\end{document}
